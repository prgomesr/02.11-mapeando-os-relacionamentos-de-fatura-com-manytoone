package br.com.ozeano.curso.api.bb.domain.model;

import java.math.BigDecimal;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Convenio extends BaseEntity {

	private String numeroContrato;
	private String carteira;
	private String variacaoCarteira;
	private BigDecimal jurosPorcentagem;
	private BigDecimal multaPorcentagem;
	private Conta conta;
	
}
